deplay:
	mkdir -p build
	util/tweak.py env/dev.env deploy.yaml dev
	util/tweak.py env/staging.env deploy.yaml staging
	util/tweak.py env/prod.env deploy.yaml prod

docker:
	util/tweak.py env/dev.env Dockerfile.template dev
