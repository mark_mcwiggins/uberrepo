#!/usr/bin/env python3

import os
import sys
import subprocess
import time

# now run from 'util' directory writing to 'build' directory with environment files in 'env' directory

whatenv = sys.argv[1]
clustname = sys.argv[2]

files = ['cluster-create.sh', 'deploy.yaml', 'service443.yaml', 'ssl-cert.yaml', 'ingress.yaml']

for f in files:
    subprocess.run(['util/tweak.py', 'env/' + whatenv + '.env', f, whatenv])


subprocess.run([f'build/cluster-create.{whatenv}.sh', clustname])
time.sleep(30)
subprocess.run(['kubectl', 'create', 'namespace',  'seven'])

subprocess.run(['kubectl', 'create', 'secret', 'generic', 'secrets-manager-key', '--from-file=key.json=clearspring-dev-075e1fd85f9f.json', '-n', 'seven'])
subprocess.run(['kubectl', 'apply', '-f', "build/deploy." + whatenv + ".yaml", "-n",  "seven"])
subprocess.run(['kubectl', 'apply', '-f', "build/service443." + whatenv + ".yaml", "-n",  "seven"])
subprocess.run(['kubectl', 'apply', '-f', "build/ssl-cert." + whatenv + ".yaml", "-n",  "seven"])
subprocess.run(['kubectl', 'apply', '-f', "build/ingress." + whatenv + ".yaml", "-n",  "seven"])

time.sleep(300)
os.system("kubectl get ing -A")

