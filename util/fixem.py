#!/usr/bin/env python3
"usage: fixem.py oldval newval -- runs on every file in the current directory, only changing ones where there's a match"

import os
import re
import sys

def fix(file, oldval, newval):
    if file == 'testit.sh':
        return
    f = open(file)
    outfile = file + '.out'
    fout = open(outfile, 'w')
    tweaked = False
    for line in f:
        (newline, n) = re.subn(oldval, newval, line)
        if n > 0:
            tweaked = True
            line = newline
        fout.write(line)
    fout.close()
    if tweaked:
        os.rename(outfile,file)
    else:
        os.remove(outfile)
    
def main(oldval, newval):
    for file in os.listdir():
        if os.path.isfile(file):
            print(file)
            fix(file, oldval, newval)

if __name__ == "__main__":
    oldval = sys.argv[1]
    newval = sys.argv[2]
    main(oldval, newval)
