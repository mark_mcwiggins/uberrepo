#!/usr/bin/env python3

import sys
import os
import re

ENVFILE = sys.argv[1]
YAMLFILE = sys.argv[2]
WHATENV = sys.argv[3]

ENV = {}

def envread():
    envfile = open(ENVFILE)
    for line in envfile:
        try:
            (name,eq,val) = line.rstrip().split()
            ENV[name] = val
        except:
            break
    print(ENV)

def tweaker():
    infile = open(YAMLFILE)
    if YAMLFILE == "Dockerfile.template":
        outfile = "Dockerfile"
    else:
        (rootfilename,suffix) = YAMLFILE.split('.')
        outfile = f'build/{rootfilename}.{WHATENV}.{suffix}'
    out = open(outfile,'w')
    for line in infile:
        for key in ENV.keys():
            if re.search(key,line):
                line = re.sub('%%'+key+'%%', ENV[key], line)
                break
        out.write(line)
    out.close()
    os.chmod(outfile, 0o0755)
    
    
    
envread()
tweaker()

